window.$ = window.jQuery = require('jquery/dist/jquery.js');

require('slick-carousel/slick/slick.js');

//require('bootstrap-sass/assets/javascripts/bootstrap/affix.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/alert.js');
require('bootstrap-sass/assets/javascripts/bootstrap/button.js');
require('bootstrap-sass/assets/javascripts/bootstrap/carousel.js');
require('bootstrap-sass/assets/javascripts/bootstrap/collapse.js');
require('bootstrap-sass/assets/javascripts/bootstrap/dropdown.js');
require('bootstrap-sass/assets/javascripts/bootstrap/modal.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/popover.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js');
require('bootstrap-sass/assets/javascripts/bootstrap/tab.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/tooltip.js');
require('bootstrap-sass/assets/javascripts/bootstrap/transition.js');


require('moment/min/moment-with-locales.min.js');
require('bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');

$(document).on('ready', function () {

  $(function () {
    $('#datetimepicker1').datetimepicker({
      inline: true
      , useCurrent: false
      , format: 'D/M/Y'
      , locale: 'pt-br'
    });
    $('#datetimepicker2').datetimepicker({
      format: 'LT',
      locale: 'pt-br'
    });
  });

  // Slick carousel init
  $('.slick').slick({
    dots: true
    , infinite: false
    , speed: 300
    , slidesToShow: 4
    , slidesToScroll: 4
    , responsive: [
      {
        breakpoint: 1024
        , settings: {
          slidesToShow: 3
          , slidesToScroll: 3
          , infinite: true
          , dots: true
        }
      }


      , {
        breakpoint: 600
        , settings: {
          slidesToShow: 2
          , slidesToScroll: 2
        }
      }


      , {
        breakpoint: 480
        , settings: {
          slidesToShow: 1
          , slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  $('.used-slider').slick({
    slidesToShow: 1
    , slidesToScroll: 1
    , arrows: false
    , fade: true
    , asNavFor: '.used-carousel'
  });
  $('.used-carousel').slick({
    slidesToShow: 3
    , slidesToScroll: 1
    , asNavFor: '.used-slider'
    , centerMode: true
    , focusOnSelect: true
  });

  $('.car-gallery').slick({
    infinite: true
    , slidesToShow: 3
    , slidesToScroll: 1
    , responsive: [
      {
        breakpoint: 768
        , settings: {
          slidesToShow: 1
        , }
    }]
  , });

  // New Car Specs table

  // Bootstrap tabs init
  $('.nav-tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });
  $('.cat-panel').hide();

  $('.accordion').find('.cat-title').click(function (f) {
    f.preventDefault();
    var cat = $(this).attr('id');
    var acc = '#panel-' + cat;
    if ($(acc).hasClass('active')) {
      $(acc).slideUp().removeClass('active');
    } else {
      $('.cat-panel').slideUp().removeClass('active');
      $(acc).slideToggle().addClass('active');
    }
  });
  // Bootstrap tabs init
  var tabs = $('.tabs .tab');
  $(tabs).on('click', function (e) {
    e.preventDefault();

    var siblingTabs = $(this).closest('.tabs').find('.tab');

    siblingTabs.removeClass('active').find('a').each(function (i, el) {
      $($(el).attr('href')).hide();
    });

    $($(this).find('a').attr('href')).show();
    $(this).addClass('active');
  });

  // Yamm init
  $('.yamm .dropdown-menu').on('click', function (e) {
    e.stopPropagation();
  });

  var qtdActive = 3;
  $('.models-open input').click(function (e) {
    var target = $(this).attr('getModel');
    var active = $(this).attr('getActive');
    if (qtdActive == 3) {
      $(this).prop('checked', false);
    }
    if (active == 'nao' && qtdActive < 3) {
      $('.' + target).show();
      qtdActive = qtdActive + 1;
      $(this).attr('getActive', 'sim');
    }
    if (active == 'sim') {
      $('.' + target).hide();
      qtdActive = qtdActive - 1;
      //$(this).removeClass('ativado');
      $(this).attr('getActive', 'nao');
    }
  });
  $('.models-open-mobile input').click(function () {
    var target = $(this).attr('getModel');
    var active = $(this).attr('getActive');
    if (qtdActive == 3) {
      $(this).prop('checked', false);
    }
    if (active == 'nao' && qtdActive < 3) {
      $('.' + target).show();
      qtdActive = qtdActive + 1;
      //$(this).addClass('ativado');
      $(this).attr('getActive', 'sim');
    }
    if (active == 'sim') {
      $('.' + target).hide();
      qtdActive = qtdActive - 1;
      //$(this).removeClass('ativado');
      $(this).attr('getActive', 'nao');
    }
  });


});
